package pe.edu.upc.goodoffer.repository;

import pe.edu.upc.goodoffer.model.Compra;

public interface ICompraRepository extends IGenericRepo<Compra, Long> {

}
