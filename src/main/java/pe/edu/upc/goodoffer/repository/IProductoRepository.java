package pe.edu.upc.goodoffer.repository;

import java.util.List;

import pe.edu.upc.goodoffer.model.Producto;

public interface IProductoRepository extends IGenericRepo<Producto, Long> {
	
	List<Producto> findByCategoriaId(Long categoriaId);
	List<Producto> findByDescripcionLike(String descripcion);
	List<Producto> findByDescripcionContaining(String descripcion);

}
