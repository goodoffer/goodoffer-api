package pe.edu.upc.goodoffer.repository;

import pe.edu.upc.goodoffer.model.Categoria;

public interface ICategoriaRepository extends IGenericRepo<Categoria, Long> {

}
