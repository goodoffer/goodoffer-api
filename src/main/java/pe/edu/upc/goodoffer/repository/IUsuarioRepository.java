package pe.edu.upc.goodoffer.repository;

import pe.edu.upc.goodoffer.model.Usuario;

public interface IUsuarioRepository extends IGenericRepo<Usuario, Long> {
	
	Usuario findByCorreo(String correo);

}
