package pe.edu.upc.goodoffer.repository;

import pe.edu.upc.goodoffer.model.Tienda;

public interface ITiendaRepository extends IGenericRepo<Tienda, Long> {

}
