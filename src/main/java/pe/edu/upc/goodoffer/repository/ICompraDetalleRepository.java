package pe.edu.upc.goodoffer.repository;

import pe.edu.upc.goodoffer.model.CompraDetalle;

public interface ICompraDetalleRepository extends IGenericRepo<CompraDetalle, Long> {

}
