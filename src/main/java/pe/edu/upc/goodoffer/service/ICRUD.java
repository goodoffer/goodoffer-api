package pe.edu.upc.goodoffer.service;

import java.util.List;
import java.util.Optional;

public interface ICRUD<T, ID> {

	T save(T obj);
	T edit(T obj);
	List<T> findAll();
	Optional<T> findById(ID id);
	void deleteById(ID id);
	
}
