package pe.edu.upc.goodoffer.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.goodoffer.model.Usuario;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.IUsuarioRepository;
import pe.edu.upc.goodoffer.service.IUsuarioService;

@Service
public class UsuarioServiceImpl extends CRUDImpl<Usuario, Long> implements IUsuarioService {

	@Autowired
	private IUsuarioRepository repo;

	@Override
	protected IGenericRepo<Usuario, Long> getRepo() {
		return repo;
	}

	@Override
	public Usuario login(Usuario usuario) {
		if(usuario.getCorreo() == null)
			return new Usuario();
		if(usuario.getClave() == null)
			return new Usuario();
		Usuario respuesta = repo.findByCorreo(usuario.getCorreo()); 
		if(respuesta == null)
			return new Usuario();
		if(!respuesta.getClave().equals(usuario.getClave()))
			return new Usuario();
		return respuesta;
	}

}
