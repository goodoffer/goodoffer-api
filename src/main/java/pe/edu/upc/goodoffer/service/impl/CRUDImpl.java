package pe.edu.upc.goodoffer.service.impl;

import java.util.List;
import java.util.Optional;

import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.service.ICRUD;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {

	protected abstract IGenericRepo<T, ID> getRepo();

	@Override
	public T save(T obj) {
		return getRepo().save(obj);
	}

	@Override
	public T edit(T obj) {
		return getRepo().save(obj);
	}

	@Override
	public List<T> findAll() {
		return getRepo().findAll();
	}

	@Override
	public Optional<T> findById(ID id) {
		return getRepo().findById(id);
	}

	@Override
	public void deleteById(ID id) {
		getRepo().deleteById(id);
	}

}