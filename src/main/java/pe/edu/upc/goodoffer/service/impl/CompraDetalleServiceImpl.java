package pe.edu.upc.goodoffer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.goodoffer.model.CompraDetalle;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.ICompraDetalleRepository;
import pe.edu.upc.goodoffer.service.ICompraDetalleService;

@Service
public class CompraDetalleServiceImpl extends CRUDImpl<CompraDetalle, Long> implements ICompraDetalleService {

	@Autowired
	private ICompraDetalleRepository repo;

	@Override
	protected IGenericRepo<CompraDetalle, Long> getRepo() {
		return repo;
	}

}
