package pe.edu.upc.goodoffer.service;

import pe.edu.upc.goodoffer.model.Usuario;

public interface IUsuarioService extends ICRUD<Usuario, Long> {
	
	Usuario login(Usuario usuario);

}