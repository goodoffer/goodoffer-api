package pe.edu.upc.goodoffer.service;

import java.util.List;

import pe.edu.upc.goodoffer.model.Producto;

public interface IProductoService extends ICRUD<Producto, Long> {
	
	List<Producto> findByCategoriaId(Long categoriaId);
	List<Producto> findByDescripcionContaining(String descripcion);

}