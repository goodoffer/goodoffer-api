package pe.edu.upc.goodoffer.service;

import java.io.IOException;
import java.util.List;

import pe.edu.upc.goodoffer.dto.CategoriaDTO;
import pe.edu.upc.goodoffer.model.Categoria;

public interface ICategoriaService extends ICRUD<Categoria, Long> {
	
	List<CategoriaDTO> listWithImage() throws IOException;

}