package pe.edu.upc.goodoffer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.goodoffer.model.Producto;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.IProductoRepository;
import pe.edu.upc.goodoffer.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Long> implements IProductoService {

	@Autowired
	private IProductoRepository repo;

	@Override
	protected IGenericRepo<Producto, Long> getRepo() {
		return repo;
	}

	@Override
	public List<Producto> findByCategoriaId(Long categoriaId) {
		return repo.findByCategoriaId(categoriaId);
	}

	@Override
	public List<Producto> findByDescripcionContaining(String descripcion) {
		return repo.findByDescripcionContaining(descripcion);
	}

}
