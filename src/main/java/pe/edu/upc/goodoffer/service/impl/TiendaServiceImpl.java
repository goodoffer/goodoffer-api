package pe.edu.upc.goodoffer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.goodoffer.model.Tienda;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.ITiendaRepository;
import pe.edu.upc.goodoffer.service.ITiendaService;

@Service
public class TiendaServiceImpl extends CRUDImpl<Tienda, Long> implements ITiendaService {

	@Autowired
	private ITiendaRepository repo;

	@Override
	protected IGenericRepo<Tienda, Long> getRepo() {
		return repo;
	}

}
