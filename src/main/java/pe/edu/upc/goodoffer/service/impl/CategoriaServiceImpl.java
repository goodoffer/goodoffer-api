package pe.edu.upc.goodoffer.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;


import pe.edu.upc.goodoffer.dto.CategoriaDTO;
import pe.edu.upc.goodoffer.model.Categoria;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.ICategoriaRepository;
import pe.edu.upc.goodoffer.service.ICategoriaService;

@Service
public class CategoriaServiceImpl extends CRUDImpl<Categoria, Long> implements ICategoriaService {

	@Autowired
	private ICategoriaRepository repo;

	@Override
	protected IGenericRepo<Categoria, Long> getRepo() {
		return repo;
	}

	@Override
	public List<CategoriaDTO> listWithImage() throws IOException {
		List<CategoriaDTO> list = new ArrayList<>();
		repo.findAll().forEach(o -> list.add(new CategoriaDTO(o)));
		File resource = new ClassPathResource("images/imagen.png").getFile();
		String text = new String(Files.readAllBytes(resource.toPath()));
		list.forEach(o -> o.setImagen(text));
		return list;
	}

}
