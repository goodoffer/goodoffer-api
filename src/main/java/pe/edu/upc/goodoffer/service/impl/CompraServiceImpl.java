package pe.edu.upc.goodoffer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.repository.IGenericRepo;
import pe.edu.upc.goodoffer.repository.ICompraRepository;
import pe.edu.upc.goodoffer.service.ICompraService;

@Service
public class CompraServiceImpl extends CRUDImpl<Compra, Long> implements ICompraService {

	@Autowired
	private ICompraRepository repo;

	@Override
	protected IGenericRepo<Compra, Long> getRepo() {
		return repo;
	}

}
