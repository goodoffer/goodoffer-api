package pe.edu.upc.goodoffer.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Información de detalle de compra")
@Entity
@Table(name = "compradetalle")
public class CompraDetalle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "idcompra", nullable = false)
	private Compra compra;
	@ManyToOne
	@JoinColumn(name = "idproducto", nullable = false)
	private Producto producto;
	@Column(name = "cantidad", nullable = false)
	private int cantidad;
	@Column(name = "preciounit", precision = 10, scale = 2)
	private BigDecimal precioUnit;
	@Column(name = "subtotal", precision = 10, scale = 2)
	private BigDecimal subTotal;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Compra getCompra() {
		return compra;
	}
	public void setCompra(Compra compra) {
		this.compra = compra;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getPrecioUnit() {
		return precioUnit;
	}
	public void setPrecioUnit(BigDecimal precioUnit) {
		this.precioUnit = precioUnit;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	
	
	
}
