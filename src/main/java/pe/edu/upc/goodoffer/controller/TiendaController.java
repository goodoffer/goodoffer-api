package pe.edu.upc.goodoffer.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upc.goodoffer.model.Tienda;
import pe.edu.upc.goodoffer.service.ITiendaService;

@RestController
@RequestMapping("/tiendas")
public class TiendaController {

	@Autowired
	private ITiendaService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Tienda>> listar() {
		List<Tienda> Tiendas = new ArrayList<>();
		Tiendas = service.findAll();
		return new ResponseEntity<List<Tienda>>(Tiendas, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Tienda> listarId(@PathVariable("id") Long id) {
		Tienda c = new Tienda();
		try {
			c = service.findById(id).get();
		} catch (Exception e) {
			return new ResponseEntity<Tienda>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Tienda>(c, HttpStatus.OK);
	}

}