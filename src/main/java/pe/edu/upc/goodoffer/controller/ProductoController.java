package pe.edu.upc.goodoffer.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upc.goodoffer.model.Producto;
import pe.edu.upc.goodoffer.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listar() {
		List<Producto> productos = new ArrayList<>();
		productos = service.findAll();
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> listarId(@PathVariable("id") Long id) {
		Producto c = new Producto();
		try {
			c = service.findById(id).get();
		} catch (Exception e) {
			return new ResponseEntity<Producto>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Producto>(c, HttpStatus.OK);
	}
	
	@GetMapping(value = "/findByCategoriaId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> findByCategoriaId(@PathVariable("id") Long id) {
		List<Producto> productos = new ArrayList<>();
		try {
			productos = service.findByCategoriaId(id);
		} catch (Exception e) {
			return new ResponseEntity<List<Producto>>(productos, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}
	
	@GetMapping(value = "/findByDescripcionContaining/{descripcion}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> findByDescripcionContaining(@PathVariable("descripcion") String descripcion) {
		List<Producto> productos = new ArrayList<>();
		try {
			productos = service.findByDescripcionContaining(descripcion);
		} catch (Exception e) {
			return new ResponseEntity<List<Producto>>(productos, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}

}