package pe.edu.upc.goodoffer.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upc.goodoffer.dto.CategoriaDTO;
import pe.edu.upc.goodoffer.model.Categoria;
import pe.edu.upc.goodoffer.service.ICategoriaService;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

	@Autowired
	private ICategoriaService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Categoria>> listar() {
		List<Categoria> Categorias = new ArrayList<>();
		Categorias = service.findAll();
		return new ResponseEntity<List<Categoria>>(Categorias, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Categoria> listarId(@PathVariable("id") Long id) {
		Categoria c = new Categoria();
		try {
			c = service.findById(id).get();
		} catch (Exception e) {
			return new ResponseEntity<Categoria>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Categoria>(c, HttpStatus.OK);
	}
	
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CategoriaDTO>> listar2() throws IOException {
		List<CategoriaDTO> categorias = new ArrayList<>();
		categorias = service.listWithImage();
		return new ResponseEntity<List<CategoriaDTO>>(categorias, HttpStatus.OK);
	}

}