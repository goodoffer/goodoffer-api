package pe.edu.upc.goodoffer.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.service.ICompraService;

@RestController
@RequestMapping("/compras")
public class CompraController {

	@Autowired
	private ICompraService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Compra>> listar() {
		List<Compra> Compras = new ArrayList<>();
		Compras = service.findAll();
		return new ResponseEntity<List<Compra>>(Compras, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Compra> listarId(@PathVariable("id") Long id) {
		Compra c = new Compra();
		try {
			c = service.findById(id).get();
		} catch (Exception e) {
			return new ResponseEntity<Compra>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Compra>(c, HttpStatus.OK);
	}

	@PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Compra> registrar(@RequestBody Compra Compra) {
		Compra c = new Compra();
		try {
			c = service.save(Compra);
		} catch (Exception e) {
			return new ResponseEntity<Compra>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Compra>(c, HttpStatus.OK);
	}
	
	@PutMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Compra> actualizar(@RequestBody Compra Compra) {
		Compra c = new Compra();
		try {
			c = service.edit(Compra);
		} catch (Exception e) {
			return new ResponseEntity<Compra>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Compra>(c, HttpStatus.OK);
	}

}