package pe.edu.upc.goodoffer.dto;

import pe.edu.upc.goodoffer.model.Categoria;

public class CategoriaDTO {

	private Long id;
	private String nombre;
	private String descripcion;
	private String imagen;
	private Boolean estado;
	
	public CategoriaDTO(Long id, String nombre, String descripcion, String imagen, Boolean estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.estado = estado;
	}
	
	public CategoriaDTO(Categoria categoria) {
		super();
		this.id = categoria.getId();
		this.nombre = categoria.getNombre();
		this.descripcion = categoria.getDescripcion();
		this.imagen = categoria.getImagen();
		this.estado = categoria.getEstado();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}	
	
}
