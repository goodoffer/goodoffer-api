package pe.edu.upc.goodoffer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoodofferApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodofferApiApplication.class, args);
	}

}
